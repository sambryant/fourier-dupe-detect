use std::collections::HashMap;
use dialoguer::Confirm;
use std::io::{Write};
use std::path::PathBuf;
use rand::seq::SliceRandom;
use rand::thread_rng;

use std::fs;

pub fn run(target_dir: &str) {
  let tmp_file = "/data/static/index.html";
  let output_dir = "output";

  let mut rng = thread_rng();

  let mut paths: Vec<PathBuf> = Vec::new();
  for p in fs::read_dir(target_dir).unwrap() {
    paths.push(std::fs::canonicalize(p.unwrap().path()).unwrap());
  }

  let id_map: HashMap<usize, PathBuf> = HashMap::from(paths.into_iter().enumerate().map(|(i, p)| (i, p)).collect());
  let ids: Vec<usize> = id_map.keys().cloned().collect();

  // let mut dupes = Vec::new();
  // let mut tests = Vec::new();
  // let mut paths1 = paths.clone();
  // let mut paths2 = paths.clone();
  // paths1.shuffle(&mut rng);
  // paths2.shuffle(&mut rng);

  // let pairs = paths1.iter()
  //   .map(|p1| paths2.iter()
  //     .map(|p2| (p1, p2)

  // for p1 in paths1.iter() {
  //   for p2 in paths2.iter() {
  //     println!("Path: {:?}, {:?}", p1, p2);
  //     update_display_page(tmp_file, p1, p2);

  //     if Confirm::new()
  //       .with_prompt("Are they dupes?")
  //       .interact()
  //       .unwrap() {
  //       dupes.push((p1, p2));
  //     } else {
  //       tests.push((p1, p2));
  //     }
  //   }
  // }
}

fn update_display_page(page: &str, p1: &PathBuf, p2: &PathBuf) {
  let mut file = fs::File::create(page).unwrap();
  write!(file,
     "<!DOCTYPE html>
      <html>
        <body>
          <img src=\"testing/{}\">
          <img src=\"testing/{}\">
        </body>
      </html>",
    p1.file_name().unwrap().to_os_string().into_string().unwrap(),
    p2.file_name().unwrap().to_os_string().into_string().unwrap())
    .unwrap();
}




