#![allow(dead_code)]

use sb_rust_library::bar::get_progress_bar;
use console::style;

use engine::{
  learner::{
    binary_input_to_float,
    FourierSampleGen,
    LinearPerceptron,
    MultiLayerPerceptron,
    SampleGen,
  },
  object::{
    Object,
    ResizedImage,
    ResizedImageOptions,
  },
  signature::{
    Representation,
    Signature,
    FourierSignature,
    FourierRepresentation,
    FourierRepresentationOptions,
  }
};
use super::runner_plot::*;

const OUTPUT_DIR: &'static str = "files/learning-results";
const KEY_FILE: &'static str = "files/input/key.json";
const CACHE_DIR: &'static str = "./files/cache";

/*

Effective parameter/method combinations

Best recorded:

  0.9956 effectiveness rate achieved with:
    modes [0, 1, 2, 3] with preserved color channels filtered through log with aspect ratio
    algorithm is binary perceptron with rate 0.0001, bias 2.1, and training size 10^5

For LowModeExpansion signature that outputs [ |dF(1,1)|, |dF(2, 2)| ],
  binary perceptron with learning rate 0.0001, bias 2.1, trained on 1000 samples yields 0.97
  contin perceptron with learning rate 0.0002, bias 2.1, trained on 1000 samples yields 0.97
  MLP with model input -> [5] -> [1] and learning rate 0.01, trained on 50000 yields 0.974
*/

const LINEAR_P_WEIGHTS: [f64; 13] = [-0.02901105148471309, -0.03556952262674496, 0.029215567529491702, -0.017612142423930787, 0.002567695295349669, -0.13016683341764704, -0.09695555786034218, -0.035260163173891024, 0.05028835060661023, -0.08284012464930031, -0.016415977113739096, 0.10874459742966393, -0.0637626940836944];
const LINEAR_P_BIAS: f64 = 2.1;

pub fn train() -> Result<(), String> {
  let obj_opts = ResizedImageOptions { size: 128, };
  let rep_opts = FourierRepresentationOptions { cutoff: 6, };
  let signature = FourierSignature::new(&[0, 1, 2, 3, 4, 5])
    .include_aspect_ratio()
    .preserve_colors()
    .preserve_complex()
    .log();

  let gen = FourierSampleGen::new(CACHE_DIR, KEY_FILE, &obj_opts, &rep_opts, signature)?;

  println!("Running {}", style("binary perceptron").blue());
  train_binary_perceptron(&gen)?;
  println!("Running {}", style("MLP").blue());
  train_mlp(&gen);
  // println!("Running {}", style("continuous perceptron").blue());
  // train_continuous_perceptron(&inputs)?;
  // scan_mlp_space(&inputs);
  Ok(())
}

pub fn check_trained() -> Result<(), String> {
  let obj_opts = ResizedImageOptions { size: 128, };
  let rep_opts = FourierRepresentationOptions { cutoff: 8, };
  let signature = FourierSignature::new(&[0, 1, 2, 3])
    .include_aspect_ratio()
    .preserve_colors()
    .log();

  let gen = FourierSampleGen::new(CACHE_DIR, KEY_FILE, &obj_opts, &rep_opts, signature)?;

  check_binary_perceptron(&gen, LINEAR_P_BIAS, &LINEAR_P_WEIGHTS)
}

pub fn check_signature_valid() {
  let obj_opts = ResizedImageOptions { size: 128 };
  let rep_opts = FourierRepresentationOptions { cutoff: 10, };
  let obj = ResizedImage::new(1, obj_opts, "./test.png").unwrap();
  let rep = FourierRepresentation::new(&obj, rep_opts);

  let img = rep.image_from_representation();
  img.save("test-recreate.png").unwrap();

  let mut sig: Vec<f64> = (0..4).clone()
    .map(|k| (0..3).clone()
      .map(|c| {
        let cm = rep.coefficient(k, k)[c];
        vec![cm.x, cm.y]
      })
      .flatten()
      .collect::<Vec<f64>>()
    )
    .flatten()
    .collect::<Vec<f64>>();
  sig.push(rep.aspect_ratio);
  println!("sig2 = {:?}", sig);

  let xr = obj.nonzero_x_range();
  let yr = obj.nonzero_y_range();
  println!("Nonzero x, y: ({}, {}), ({}, {})", xr.start, xr.end, yr.start, yr.end);

  println!("Zero mode: {}", rep.coefficient(0, 0)[0].x);

  // let p = obj.get_pixel(0, 0);
  // println!("Random pixel: {}, {}, {}", p[0], p[1], p[2]);
}

fn train_mlp<A, B, C>(sample_gen: &SampleGen<A, B, C>) -> MultiLayerPerceptron
where A: Representation<Options = B>,
      B: Clone,
      C: Signature<A> {
  let rate = 0.01;
  let model_shape = vec![5];
  let train_size = 100000;

  let inputs = sample_gen.get_sample_set(train_size);
  let continous_inputs = binary_input_to_float(&inputs);

  let nn = MultiLayerPerceptron::new(rate, model_shape);
  nn.run(&continous_inputs, train_size as i64);

  nn
}

fn scan_mlp_space<T: Clone>(inputs: &[(Vec<f64>, bool, T)]) {
  let rates = [0.01, 0.001, 0.0001];
  let train_sizes = [10000];
  let model_shapes = [
    vec![5], vec![10],
    vec![5, 5], vec![10, 10],
    vec![5, 5, 5], vec![10, 10, 10],
  ];

  let size = rates.len() * train_sizes.len() * model_shapes.len();
  let bar = get_progress_bar("Searching param space", size as u64, true);

  let continous_inputs = binary_input_to_float(inputs);

  let mut results = Vec::new();
  for rate in rates.iter() {
    for train_size in train_sizes.iter() {
      for model_shape in model_shapes.iter() {
        let success = MultiLayerPerceptron::new(*rate, model_shape.to_vec())
          .quiet()
          .run(&continous_inputs, *train_size);
        results.push((train_size, rate, model_shape, success));
        bar.inc(1);
      }
    }
  }
  bar.finish_and_clear();

  let mut max = 0.0;
  let mut max_i = 0;
  for (i, (_, _, _, success)) in results.iter().enumerate() {
    if *success > max {
      max_i = i;
      max = *success;
    }
  }
  println!("Highest found success was: {}", max);
  println!("Parameters:\n  rate: {}\n  size: {}\n  model: {:?}", results[max_i].1, results[max_i].0, results[max_i].2);
}

fn check_binary_perceptron<A, B, C>(sample_gen: &SampleGen<A, B, C>, bias: f64, weights: &[f64]) -> Result<(), String>
where A: Representation<Options = B>,
      B: Clone,
      C: Signature<A> {
  let inputs = sample_gen.get_full_set();
  let mut nn = LinearPerceptron::new(0.0, bias, inputs[0].0.len());
  nn.set_weights(weights);

  let (a, false_pos, false_neg) = nn.check_binary(&inputs);
  println!("Success rate: {}", a);

  println!("False Positives:");
  for id in false_pos.iter() {
    println!("  {:?}", id);
  }
  println!("False Negatives:");
  for id in false_neg.iter() {
    println!("  {:?}", id);
  }
  // plot_weights_1d(OUTPUT_DIR, "binary_perceptron_learning_weights_ar.png", inputs, &nn.weights, bias);
  // plot_weights(OUTPUT_DIR, "binary_perceptron_learning_weights.png", inputs, &nn.weights, bias);
  Ok(())
}

fn train_binary_perceptron<A, B, C>(sample_gen: &SampleGen<A, B, C>) -> Result<(), String>
where A: Representation<Options = B>,
      B: Clone,
      C: Signature<A> {
  let rate = 0.0001;
  let bias = 1.5;
  let train_size = 100000;

  let inputs = sample_gen.get_sample_set(train_size);

  let mut nn = LinearPerceptron::new(rate, bias, inputs[0].0.len());
  nn.train_binary(&inputs, true);
  let (_, false_pos, false_neg) = nn.check_binary(&inputs);

  println!("Weights = {:?}", &nn.weights);

  println!("False positive rate: {} ({})", (false_pos.len() as f32) / ((inputs.len() as f32 / 2.0)), false_pos.len());
  println!("False negative rate: {} ({})", (false_neg.len() as f32) / ((inputs.len() as f32 / 2.0)), false_neg.len());
  plot_weights(OUTPUT_DIR, "binary_perceptron_learning_weights.png", &inputs, &nn.weights, bias);

  Ok(())
}

fn train_continuous_perceptron<A, B, C>(sample_gen: &SampleGen<A, B, C>) -> Result<(), String>
where A: Representation<Options = B>,
      B: Clone,
      C: Signature<A> {
  let rate = 0.0001;
  let bias = 2.1;
  let train_size = 1000;

  let inputs = sample_gen.get_sample_set(train_size);
  let continous_inputs = binary_input_to_float(&inputs);

  let mut nn = LinearPerceptron::new(rate, bias, inputs[0].0.len());
  nn.train_float(&continous_inputs, train_size as u32, true);
  nn.check(&continous_inputs);

  println!("Weights = {:?}", nn.weights);

  plot_weights(OUTPUT_DIR, "continuous_perceptron_learning_weights.png", &inputs, &nn.weights, bias);

  Ok(())
}
