use rand::seq::SliceRandom;
use rand::thread_rng;
use rand::Rng;
use serde::Deserialize;
use std::collections::HashMap;
use std::fs::read_to_string;

use crate::resource::*;

#[derive(Deserialize)]
struct LabeledFilesRawJSON {
  pub files: Vec<(usize, String)>,
  pub dupes: Vec<Vec<usize>>,
  pub tests: Vec<Vec<usize>>,
}

pub struct LabeledFiles {
  pub ids: Vec<ResourceId>,
  pub files: HashMap<ResourceId, String>,
  pub dupe_matrix: Vec<Vec<bool>>, // Indexed by ResourceId so important they be mostly sequential
  pub dupe_pairs: Vec<[ResourceId; 2]>,
  pub test_pairs: Vec<[ResourceId; 2]>,
  // // pub dupes: Vec<Vec<ResourceId>>,
  // pub dupe_map: HashMap<ResourceId, HashSet<ResourceId>>,
  // // pub tests: Vec<Vec<ResourceId>>,
}

#[derive(Debug)]
pub enum Error {
  ReadError(std::io::Error),
  ParseError(serde_json::Error),
  InconsistencyError(String),
}

// BOOKMARK: Something is very broken here. The number of dupes changes eveyr time I call it.

// This class needs to be rewritten

impl LabeledFiles {

  pub fn from_key(key_file: &str) -> Result<LabeledFiles, Error> {
    use Error::*;
    let contents = read_to_string(key_file)
      .map_err(|e| ReadError(e))?;
    let raw: LabeledFilesRawJSON = serde_json::from_str(&contents)
      .map_err(|e| ParseError(e))?;

    let files: HashMap<ResourceId, String> = raw.files.iter().cloned().collect();
    let ids: Vec<ResourceId> = files.keys().cloned().collect();
    let max_id = *ids.iter().max().unwrap() + 1;

    let mut dupe_matrix: Vec<Vec<bool>> = (0..max_id)
      .map(|id1| (0..max_id)
        .map(|id2| id1 == id2)
        .collect())
      .collect();

    for dupes in raw.dupes.iter() {
      for id1 in dupes.iter() {
        for id2 in dupes.iter() {
          dupe_matrix[*id1][*id2] = true;
          dupe_matrix[*id2][*id1] = true;
        }
      }
    }
    let mut dupe_pairs = Vec::<[ResourceId; 2]>::new();
    let mut test_pairs = Vec::<[ResourceId; 2]>::new();
    for id1 in ids.iter() {
      for id2 in ids.iter() {
        if id1 != id2 {
          if dupe_matrix[*id1][*id2] {
            dupe_pairs.push([*id1, *id2]);
          } else {
            test_pairs.push([*id1, *id2]);
          }
        }
      }
    }

    println!("Loaded input set with {} files, {} dupe pairs, and {} tests", ids.len(), dupe_pairs.len(), test_pairs.len());

    Ok(LabeledFiles { ids, files, dupe_matrix, dupe_pairs, test_pairs })
  }

  pub fn get_full_set(&self) -> Vec<([ResourceId; 2], bool)> {
    let mut pairs = Vec::new();
    for pair in self.dupe_pairs.iter() {
      pairs.push((pair.clone(), true));
    }
    for pair in self.test_pairs.iter() {
      pairs.push((pair.clone(), false));
    }
    pairs.shuffle(&mut thread_rng());
    pairs
  }

  pub fn get_sample_set(&self, size: usize) -> Vec<([ResourceId; 2], bool)> {
    let num_dupes = self.dupe_pairs.len();
    let num_tests = self.test_pairs.len();
    println!("Building sample set of size {} from {} dupes and {} tests", size, num_dupes, num_tests);

    let mut rng = thread_rng();
    let mut pairs = Vec::new();
    while pairs.len() < (size / 2) {
      pairs.push((self.dupe_pairs[rng.gen_range(0..num_dupes)].clone(), true));
    }
    while pairs.len() < size {
      pairs.push((self.test_pairs[rng.gen_range(0..num_tests)].clone(), false));
    }
    pairs.shuffle(&mut rng);
    pairs
  }
}

impl From<Error> for String {
  fn from(e: Error) -> String {
    use Error::*;
    match e {
      ReadError(e) => format!("Couldnt read labeled files key file\nCause: {:?}", e),
      ParseError(e) => format!("Couldnt parse labeled files key file\nCause: {:?}", e),
      InconsistencyError(e) => format!("Duplicate key file was found to contain inconsistent information: {}", e),
    }
  }
}

// pub fn test() {
//   // let inps = LabeledFiles::from_key("input-files/ready/key.json").unwrap();
//   // println!("Total: {}", inps.get_full_set().len());
//   // for p in inps.dupe_pairs.iter() {
//   //   println!("dupe: {:?}", p);
//   // }
//   // for p in inps.test_pairs.iter() {
//   //   println!("test: {:?}", p);
//   // }
//   // println!("Total: {}", inps.get_sample_set(100).len());
// }