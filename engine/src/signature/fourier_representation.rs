use image::{RgbImage, Rgb};
use num::complex::Complex;
use serde::{Deserialize, Serialize};

use derives::{SerializedResource, Resource};
use crate::resource::{Resource, ResourceId, SerializedResource};
use crate::object::ResizedImage;

use super::Representation;

#[derive(Serialize, Deserialize, Resource, SerializedResource)]
#[option_struct = "FourierRepresentationOptions"]
#[filename_generator = "filename_generator"]
pub struct FourierRepresentation {
  pub id: usize,
  pub dims: usize, // max(width, height) of input image
  pub aspect_ratio: f64,
  coefs: Vec<Vec<[Vector; 3]>>, // indexed by [kx][ky][RGB][real | imag],
  // pub coefs: Vec<f64>, // indexed by: [ky][kx][RGB][real | imag]
  options: FourierRepresentationOptions,
  pixel_size: usize,
  row_size: usize,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct FourierRepresentationOptions {
  pub cutoff: u32
}

#[derive(Clone, Copy, Serialize, Deserialize)]
pub struct Vector {
  pub x: f64,
  pub y: f64,
}

fn filename_generator(id: ResourceId, options: &FourierRepresentationOptions) -> String {
  format!("fourier_cutoff={}_id={}.signature", options.cutoff, id)
}

impl Representation for FourierRepresentation {
  type Object = ResizedImage;

  fn new(input: &ResizedImage, options: Self::Options) -> Self {

    let n = input.get_options().size as usize;
    let pixel_size = 2 * 3;
    let row_size = pixel_size * n;
    // let mut coefs = vec![0.0; (row_size * n) as usize];
    let cutoff = options.cutoff as usize;

    let coefs = (0..cutoff).map(|kx| {
      (0..cutoff).map(|ky| {
        let factor_kx = 2.0 * 3.14159 / (n as f64) * (kx as f64);
        let factor_ky = 2.0 * 3.14159 / (n as f64) * (ky as f64);

        let mut entry = [Vector::from([0.0, 0.0]); 3];

        for c in 0..3 {
          let cmpl: Complex<f64> = input.nonzero_y_range()
            .map(|y| input.nonzero_x_range()
              .map(|x| {
                let f = input.get_pixel(x, y)[c];
                f * Complex::new(0.0, -factor_kx * (x as f64) - factor_ky * (y as f64)).exp()
              })
              .sum::<Complex<f64>>())
            .sum();
          entry[c] = [cmpl.re, cmpl.im].into();
        }
        entry
      }).collect()
    }).collect();

    FourierRepresentation {
      id: input.get_id(),
      dims: n,
      aspect_ratio: input.aspect_ratio,
      coefs,
      options,
      pixel_size,
      row_size,
    }

  }
}

impl FourierRepresentation {
  pub fn nonzero_kx_range(&self) -> std::ops::Range<usize> {
    0..(self.options.cutoff as usize)
  }

  pub fn nonzero_ky_range(&self) -> std::ops::Range<usize> {
    0..(self.options.cutoff as usize)
  }

  pub fn coefficient(&self, kx: usize, ky: usize) -> [Vector; 3] {
    self.coefs[kx][ky]
  }

  pub fn image_from_representation(&self) -> RgbImage {
    Self::pixels_to_img(
      Self::spatial_rep_to_pixels(self.reverse_ft()), 1)
  }

  fn reverse_ft(&self) -> Vec<Vec<[f64; 3]>> {
    let n = self.dims;
    let norm = 1.0 / ((n * n) as f64);
    (0..n).map(|y| {
      (0..n).map(|x| {

        let factor_x = 2.0 * 3.14159 / (n as f64) * (x as f64);
        let factor_y = 2.0 * 3.14159 / (n as f64) * (y as f64);
        let mut sum = [Complex::new(0.0, 0.0), Complex::new(0.0, 0.0), Complex::new(0.0, 0.0)];

        for ky in self.nonzero_ky_range() {
          for kx in self.nonzero_kx_range() {
            let vals = self.coefficient(kx, ky);
            for i in 0..3 {
              let f = vals[i];
              let a = Complex::new(0.0, factor_x * (kx as f64) + factor_y * (ky as f64)).exp();
              sum[i] += Complex::new(f.x * a.re - f.y * a.im, f.x * a.im + f.y * a.re);
            }
          }
        }
        [norm * sum[0].re, norm * sum[1].re, norm * sum[2].re]
      }).collect()
    }).collect()
  }

  fn pixels_to_img(values: Vec<Vec<[u8; 3]>>, scale_factor: u32) -> RgbImage {
    let n_x = values.len() as u32;
    let n_y = values[0].len() as u32;
    let mut img = RgbImage::new(n_x, n_y);

    for x in 0..n_x {
      for y in 0..n_y {
        for xi in 0..scale_factor {
          for yi in 0..scale_factor {
            img.put_pixel(x * scale_factor + xi, y * scale_factor + yi, Rgb(values[y as usize][x as usize]));
          }
        }
      }
    }
    img
  }

  fn spatial_rep_to_pixels(spatial_representation: Vec<Vec<[f64; 3]>>) -> Vec<Vec<[u8; 3]>> {
    spatial_representation.into_iter()
      .map(|values| values.into_iter()
        .map(|v| {
          [(v[0] * 256.0) as u8, (v[1] * 256.0) as u8, (v[2] * 256.0) as u8]
        })
        .collect()
      )
      .collect()
  }
}

impl Vector {
  pub fn norm(&self) -> f64 {
    self.norm2().sqrt()
  }

  pub fn norm2(&self) -> f64 {
    self.x * self.x + self.y * self.y
  }
}

impl std::ops::Sub for Vector {
  type Output = Vector;

  fn sub(self, other: Self) -> Self::Output {
    Vector { x: self.x - other.x, y: self.y - other.y }
  }

}

impl From<[f64; 2]> for Vector {
  fn from([x, y]: [f64; 2]) -> Vector {
    Vector { x, y }
  }
}
