use super::fourier_representation::*;
use super::signature::*;


pub struct FourierSignature {
  modes: Vec<usize>,
  include_aspect_ratio: bool,
  preserve_complex: bool,
  preserve_colors: bool,
  log: bool,
}

fn log_mapped(value: f64) -> f64 {
  if value == 0.0 {
    -700.0 // Chosen to be close to `log(smallest magnitude f64)`
  } else {
    value.ln()
  }
}

impl FourierSignature {

  pub fn new(modes: &[usize]) -> FourierSignature {
    FourierSignature {
      modes: Vec::from(modes),
      include_aspect_ratio: false,
      preserve_colors: false,
      preserve_complex: false,
      log: false,
    }
  }

  pub fn include_aspect_ratio(mut self) -> Self {
    self.include_aspect_ratio = true;
    self
  }

  pub fn preserve_colors(mut self) -> Self {
    self.preserve_colors = true;
    self
  }

  pub fn preserve_complex(mut self) -> Self {
    self.preserve_complex = true;
    self
  }

  pub fn log(mut self) -> Self {
    self.log = true;
    self
  }

  pub fn linear(mut self) -> Self {
    self.log = false;
    self
  }

}

impl Signature<FourierRepresentation> for FourierSignature {

  fn size(&self) -> usize {
    self.modes.len()
      * if self.preserve_complex { 2 } else { 1 }
      * if self.preserve_colors { 3 } else { 1 }
  }

  fn compute(&self, r1: &FourierRepresentation, r2: &FourierRepresentation) -> Vec<f64> {
    let mut result: Vec<f64> = if !self.preserve_complex && !self.preserve_colors {
      self.modes.iter().cloned()
        .map(|k| {
          let c0 = r1.coefficient(k, k)[0] - r2.coefficient(k, k)[0];
          let c1 = r1.coefficient(k, k)[1] - r2.coefficient(k, k)[1];
          let c2 = r1.coefficient(k, k)[2] - r2.coefficient(k, k)[2];
          c0.norm2() + c1.norm2() + c2.norm2()
        })
        .map(|v| if self.log { log_mapped(v) } else { v })
        .collect()
    } else {
      self.modes.iter().cloned()
        .map(|k| {
          let c0 = r1.coefficient(k, k)[0] - r2.coefficient(k, k)[0];
          let c1 = r1.coefficient(k, k)[1] - r2.coefficient(k, k)[1];
          let c2 = r1.coefficient(k, k)[2] - r2.coefficient(k, k)[2];

          if self.preserve_complex && !self.preserve_colors {
            vec![
              c0.x * c0.x + c1.x * c1.x + c2.x + c2.x,
              c0.y * c0.y + c1.y * c1.y + c2.y + c2.y,
            ]
          } else if !self.preserve_complex && self.preserve_colors {
            vec![
              c0.norm2(),
              c1.norm2(),
              c2.norm2(),
            ]
          } else {
            vec![
              c0.x * c0.x,
              c0.y * c0.y,
              c1.x * c1.x,
              c1.y * c1.y,
              c2.x * c2.x,
              c2.y * c2.y,
            ]
          }
        })
        .flatten()
        .map(|v| if self.log { log_mapped(v) } else { v })
        .collect()
    };
    if self.include_aspect_ratio {
      result.push((r1.aspect_ratio - r2.aspect_ratio).abs());
    }
    result
    // let deltas: Vec<Vec<Complex<f64>>> = self.modes.iter().cloned()
    //   .map(|k| {
    //     (0..3).map(|c| { r1.coefficient(k, k)[c] - r2.coefficient(k, k)[c] }).collect()
    //   })
    //   .collect();

    // if self.preserve_complex {
    //   deltas
    //     .iter()
    //     .map(|delta_k| delta_k
    //       .iter()
    //       .map(|cmplx| (cmplx.re * cmplx.re + cmplx.im * cmplx.im))
    //       .map(|norm| if self.log { log_mapped(norm) } else { norm })
    //       .collect::<Vec<f64>>())
    //     .flatten()
    //     .collect()
    // } else {
    //   deltas
    //     .iter()
    //     .map(|delta_k| delta_k
    //       .iter()
    //       .map(|cmplx| vec![cmplx.re * cmplx.re, cmplx.im * cmplx.im]
    //         .iter()
    //         .map(|value| if self.log { log_mapped(*value) } else { *value })
    //         .collect::<Vec<f64>>())
    //       .flatten()
    //       .collect::<Vec<f64>>())
    //     .flatten()
    //     .collect()
    // }
  }

}


// pub struct LowModeSignature {}
// pub struct LinearModeSignature {}

// impl Signature<FourierRepresentation> for LinearModeSignature {

//   fn signature(rep1: &FourierRepresentation, rep2: &FourierRepresentation) -> Vec<f64> {
//     // |Aspect ratio|, |0 modes|^2, |1 modes|^2, |2 modes|^2
//     let mut d1: f64 = (0..3)
//       .map(|c| rep1.coefficient(1, 1)[c] - rep2.coefficient(1, 1)[c])
//       .map(|value| value.re * value.re + value.im * value.im)
//       .sum::<f64>();
//     let mut d2: f64 = (0..3)
//       .map(|c| rep1.coefficient(2, 2)[c] - rep2.coefficient(2, 2)[c])
//       .map(|value| value.re * value.re + value.im * value.im)
//       .sum::<f64>();
//     vec![d1, d2]
//   }

//   fn size() -> usize {
//     2
//   }
// }

// impl Signature<FourierRepresentation> for LowModeSignature {

//   fn signature(rep1: &FourierRepresentation, rep2: &FourierRepresentation) -> Vec<f64> {
//     // |Aspect ratio|, |0 modes|^2, |1 modes|^2, |2 modes|^2
//     let ar = (rep1.aspect_ratio - rep2.aspect_ratio).abs();
//     let mut d0: f64 = (0..3)
//       .map(|c| rep1.coefficient(0, 0)[c] - rep2.coefficient(0, 0)[c])
//       .map(|value| value.re * value.re + value.im * value.im)
//       .sum::<f64>();
//     let mut d1: f64 = (0..3)
//       .map(|c| rep1.coefficient(1, 1)[c] - rep2.coefficient(1, 1)[c])
//       .map(|value| value.re * value.re + value.im * value.im)
//       .sum::<f64>();
//     let mut d2: f64 = (0..3)
//       .map(|c| rep1.coefficient(2, 2)[c] - rep2.coefficient(2, 2)[c])
//       .map(|value| value.re * value.re + value.im * value.im)
//       .sum::<f64>();
//     let mut d3: f64 = (0..3)
//       .map(|c| rep1.coefficient(3, 3)[c] - rep2.coefficient(3, 3)[c])
//       .map(|value| value.re * value.re + value.im * value.im)
//       .sum::<f64>();
//     if d0 == 0.0 {
//       d0 = 0.00001;
//     }
//     if d1 == 0.0 {
//       d1 = 0.00001;
//     }
//     if d2 == 0.0 {
//       d2 = 0.00001;
//     }
//     if d3 == 0.0 {
//       d3 = 0.00001;
//     }
//     vec![d1.ln(), d2.ln()]
//   }

//   fn size() -> usize {
//     2
//   }
// }

// pub struct ExpandedModeSignature {}

// fn reg(v: f64) -> f64 {
//   if v == 0.0 {
//     -100000.0
//   } else {
//     v.ln()
//   }
// }

// impl Signature<FourierRepresentation> for ExpandedModeSignature {


//   fn signature(rep1: &FourierRepresentation, rep2: &FourierRepresentation) -> Vec<f64> {
//     // |Aspect ratio|, |0 modes|^2, |1 modes|^2, |2 modes|^2
//     let ar = (rep1.aspect_ratio - rep2.aspect_ratio).abs();
//     let reals: Vec<f64> = (0..4).map(
//       |k| reg((0..3)
//         .map(|c| rep1.coefficient(k, k)[c] - rep2.coefficient(k, k)[c])
//         .map(|value| value.re * value.re)
//         .sum::<f64>())
//       )
//       .collect();
//     let imags: Vec<f64> = (0..4).map(
//       |k| reg((0..3)
//         .map(|c| rep1.coefficient(k, k)[c] - rep2.coefficient(k, k)[c])
//         .map(|value| value.im * value.im)
//         .sum::<f64>())
//       )
//       .collect();

//     vec![ar, reals[0], imags[0], reals[1], imags[1], reals[2], imags[2], reals[3], imags[3]]
//   }

//   fn size() -> usize {
//     9
//   }
// }





