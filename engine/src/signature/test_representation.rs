use rand::Rng;
use serde::{Deserialize, Serialize};

use derives::{SerializedResource, Resource};
use crate::input::Object;
use crate::signature::Representation;
use crate::resource::{
  Resource,
  ResourceId,
  SerializedResource,
};

#[derive(Clone, Serialize, Deserialize)]
pub struct TestOptions {
  pub dims: usize,
}

#[derive(Serialize, Deserialize, Resource, SerializedResource)]
#[option_struct = "TestOptions"]
#[filename_generator = "input_filename_generator"]
pub struct TestObject {
  id: usize,
  options: TestOptions,
  pub values: Vec<f64>,
}

fn input_filename_generator(id: ResourceId, options: &TestOptions) -> String {
  format!("test_dims={}_id={}.input", options.dims, id)
}

impl Object for TestObject {
  type ComputeError = ();

  fn new(id: ResourceId, options: TestOptions, _filename: &str) -> Result<Self, ()> {
    let mut rng = rand::thread_rng();
    let mut values = Vec::new();
    for _ in 0..options.dims {
      values.push(rng.gen())
    }
    Ok(TestObject { id, options, values, })
  }
}


#[derive(Serialize, Deserialize, Resource, SerializedResource)]
#[option_struct = "TestOptions"]
#[filename_generator = "rep_filename_generator"]
pub struct TestRepresentation {
  id: usize,
  options: TestOptions,
  values: Vec<f64>,
}

fn rep_filename_generator(id: ResourceId, options: &TestOptions) -> String {
  format!("test_dims={}_id={}.signature", options.dims, id)
}

impl Representation for TestRepresentation {
  type Object = TestObject;

  fn new(input: &TestObject, options: Self::Options) -> Self {
    TestRepresentation { id: input.get_id(), options, values: input.values.clone(), }
  }
}
