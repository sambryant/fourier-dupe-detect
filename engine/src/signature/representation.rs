use sb_rust_library::bar::get_progress_bar;
use std::collections::HashMap;
use std::fmt::Debug;
use std::fs;
use std::path::Path;

use crate::object::{Object, ObjectSet};
use crate::resource::{CacheableResource, Resource, ResourceId};


pub enum Error<T, U> {
  CacheLoadError(ResourceId, String, T),
  CacheSaveError(ResourceId, String, U),
}


pub trait Representation: CacheableResource {
  type Object: Object;

  fn new(input: &Self::Object, options: Self::Options) -> Self;

  fn from_stored(id: ResourceId, options: Self::Options, cache_dir: &str) -> Result<Self, ()> {
    if Self::has_cache_file(id, &options, cache_dir) {
      Self::load_cache(id, options, cache_dir)
        .map_err(|e| {
          eprintln!("Bad cache file for {}: {:?}", id, e);
          ()
        })
    } else {
      eprintln!("Missing cache file for {}", id);
      Err(())
    }
  }

  fn from_stored_or_compute(input: &Self::Object, options: Self::Options, cache_dir: &str) -> Result<Self, Error<Self::LoadError, Self::SaveError>> {
    use Error::*;
    if Self::has_cache_file(input.get_id(), &options, cache_dir) {
      Self::load_cache(input.get_id(), options, cache_dir)
        .map_err(|e| CacheLoadError(input.get_id(), cache_dir.to_string(), e))
    } else {
      Self::new(input, options)
        .save_cache(cache_dir)
        .map_err(|e| CacheSaveError(input.get_id(), cache_dir.to_string(), e))
    }
  }

  fn load_cached_set(ids: &[ResourceId], options: &Self::Options, cache_dir: &str, verbose: bool) -> Result<RepresentationSet<Self>, ()> {
    // Nice progress bar output
    let bar = get_progress_bar("Loading representations", ids.len() as u64, verbose);

    // For each input, generate the representation
    let mut reps: HashMap<ResourceId, Self> = HashMap::new();
    for id in ids.iter() {
      reps.insert(*id, Self::from_stored(*id, options.clone(), cache_dir)?);
      bar.inc(1);
    }
    bar.finish_and_clear();

    Ok(reps)
  }

  fn load_set(objs: &ObjectSet<Self::Object>, options: &Self::Options, cache_dir: &str, verbose: bool) -> Result<RepresentationSet<Self>, String> {
    // Create directory to store processed inputs in for cacheing
    if !Path::new(cache_dir).exists() {
      if let Err(e) = fs::create_dir(cache_dir) {
        eprintln!("Couldnt create cache directory for input set: {} ({:?})", cache_dir, e);
        panic!()
      }
    }

    // Nice progress bar output
    let bar = get_progress_bar("Computing representations", objs.len() as u64, verbose);

    // For each input, generate the representation
    let mut reps: HashMap<ResourceId, Self> = HashMap::new();
    for (id, obj) in objs.iter() {
      let rep = Self::from_stored_or_compute(obj, options.clone(), cache_dir)?;
      reps.insert(*id, rep);
      bar.inc(1);
    }
    bar.finish_and_clear();

    Ok(reps)
  }

}

pub type RepresentationSet<T> = HashMap<ResourceId, T>;


impl<T, U> From<Error<T, U>> for String
where T: Debug, U: Debug {
  fn from(err: Error<T, U>) -> String {
    use Error::*;
    match err {
      CacheLoadError(id, cache_dir, e) => format!("
        Unabled to read cached representation file (id={}, cache_dir={})
        Cause: {:?}", id, cache_dir, e),
      CacheSaveError(id, cache_dir, e) => format!("
          Unabled to save cached representation file (id={}, cache_dir={})
          Cause: {:?}", id, cache_dir, e),
    }
  }
}
