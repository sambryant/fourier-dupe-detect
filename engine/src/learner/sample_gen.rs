use crate::object::Object;
use crate::labeled_files::LabeledFiles;
use crate::resource::{
  Resource,
  ResourceId,
};
use crate::signature::{
  FourierRepresentation,
  FourierRepresentationOptions,
  FourierSignature,
  Representation,
  RepresentationSet,
  Signature,
};


pub struct SampleGen<A, B, C>
where A: Representation<Options = B>,
      B: Clone,
      C: Signature<A> {
  reps: RepresentationSet<A>,
  labeled_files: LabeledFiles,
  signature: C,
}

impl<A, B, C> SampleGen<A, B, C>
where A: Representation<Options = B>,
      B: Clone,
      C: Signature<A> {
  pub fn cached(cache_dir: &str, key_file: &str, rep_opts: &B, signature: C) -> Result<Self, String> {
    let inps = LabeledFiles::from_key(key_file)?;
    A::load_cached_set(&inps.ids, rep_opts, cache_dir, true)
      .map_err(|_| String::from("Error loading one of the cached representations"))
      .map(|reps| SampleGen { reps, labeled_files: inps, signature })
  }

  pub fn new(cache_dir: &str, key_file: &str, obj_opts: &<<A as Representation>::Object as Resource>::Options, rep_opts: &B, signature: C) -> Result<Self, String> {
    let inps = LabeledFiles::from_key(key_file)?;
    let objs = A::Object::load_set(&inps, obj_opts, cache_dir, true)?;
    A::load_set(&objs, rep_opts, cache_dir, true)
      .map(|reps| SampleGen { reps, labeled_files: inps, signature })
  }

  pub fn get_sample_set(&self, size: u32) -> Vec<(Vec<f64>, bool, [ResourceId; 2])> {
    self.labeled_files.get_sample_set(size as usize)
      .into_iter()
      .map(|([id1, id2], label)|
        (self.signature.compute(&self.reps[&id1], &self.reps[&id2]), label, [id1, id2]))
      .collect()
  }

  pub fn get_full_set(&self) -> Vec<(Vec<f64>, bool, [ResourceId; 2])> {
    self.labeled_files.get_full_set()
      .into_iter()
      .map(|([id1, id2], label)|
        (self.signature.compute(&self.reps[&id1], &self.reps[&id2]), label, [id1, id2]))
      .collect()
  }
}

pub type FourierSampleGen = SampleGen<FourierRepresentation, FourierRepresentationOptions, FourierSignature>;
