use sb_rust_library::bar::get_progress_bar;
use sb_rust_library::math::VectorMath;
use neuroflow::FeedForward;
use neuroflow::data::DataSet;
use neuroflow::activators::Type::Tanh;
use rand::thread_rng;
use rand::Rng;

pub fn binary_input_to_float<T: Clone>(data: &[(Vec<f64>, bool, T)]) -> Vec<(Vec<f64>, f64, T)> {
  data.iter()
    .map(|(x, d, l)| {
      if *d {
        (x.clone(), 1.0, l.clone())
      } else {
        (x.clone(), -1.0, l.clone())
      }
    })
    .collect()
}

pub struct MultiLayerPerceptron {
  pub verbose: bool,
  pub learning_rate: f64,
  pub hidden_layer_sizes: Vec<i32>,
}

impl MultiLayerPerceptron {

  pub fn new(learning_rate: f64, hidden_layer_sizes: Vec<i32>) -> MultiLayerPerceptron {
    MultiLayerPerceptron { learning_rate, hidden_layer_sizes, verbose: true }
  }

  pub fn quiet(mut self) -> Self {
    self.verbose = false;
    self
  }

  pub fn run<T>(&self, inputs: &[(Vec<f64>, f64, T)], train_size: i64) -> f64 {
    let input_size = inputs[0].0.len() as i32;

    // Build NN model
    let mut shape = Vec::new();
    shape.push(input_size);
    shape.append(&mut self.hidden_layer_sizes.clone());
    shape.push(1);
    let mut nn = FeedForward::new(&shape[..]);

    // Build data set
    let mut data = DataSet::new();
    for (x, y, _) in inputs.iter() {
      data.push(x, &[*y]);
    }

    // Train data
    nn.activation(Tanh)
     .learning_rate(self.learning_rate)
     .train(&data, train_size);

    // Check how it does
    let mut correct_count = 0;
    for (input, label, _) in inputs.iter() {
      let res = nn.calc(&input)[0];
      if (res > 0.0) == (*label > 0.0) {
        correct_count += 1;
      }
    }
    let success = (correct_count as f64) / (inputs.len() as f64);
    if self.verbose {
      println!("After training, the success rate is: {}", success);
    }
    success
  }

}

pub struct LinearPerceptron {
  pub learning_rate: f64,
  pub bias: f64,
  pub size: usize,
  pub weights: Vec<f64>,
}

impl LinearPerceptron {

  pub fn new(learning_rate: f64, bias: f64, size: usize) -> LinearPerceptron {
    LinearPerceptron { learning_rate, bias, size, weights: vec![0.0; size] }
  }

  pub fn set_weights(&mut self, weights: &[f64]) {
    self.weights = weights.to_vec();
  }

  pub fn train_binary<T>(&mut self, data: &[(Vec<f64>, bool, T)], verbose: bool) {
    let size = data.len();
    let bar = get_progress_bar("Learning", size as u64, verbose);
    for (x, d, _) in data.iter() {
      let yi = x.dot(&self.weights) + self.bias > 0.0;

      if *d != yi {
        for j in 0..self.weights.len() {
          if *d {
            self.weights[j] += self.learning_rate * x[j];
          } else {
            self.weights[j] -= self.learning_rate * x[j];
          }
        }
      }
      bar.inc(1);
    }
    bar.finish_and_clear();
  }

  pub fn train_float<T>(&mut self, data: &[(Vec<f64>, f64, T)], iterations: u32, verbose: bool) {
    let rng = &mut thread_rng();
    let size = data.len();
    let bar = get_progress_bar("Learning", iterations as u64, verbose);
    for _ in 0..iterations {
      let (x, d, _) = &data[rng.gen_range(0..size)];
      let yi = x.dot(&self.weights) + self.bias;

      for j in 0..self.weights.len() {
        self.weights[j] += self.learning_rate * (d - yi) * x[j];
      }
      bar.inc(1);
    }
    bar.finish_and_clear();
  }

  pub fn check<T: Clone>(&self, data: &[(Vec<f64>, f64, T)]) -> (f64, Vec<T>, Vec<T>) {
    let mut false_neg = Vec::new();
    let mut false_pos = Vec::new();
    let mut correct_count = 0;
    for (x, d, label) in data.iter() {
      let yi = x.dot(&self.weights) + self.bias;

      if (*d > 0.0) == (yi > 0.0) {
        correct_count += 1;
      } else if *d > 0.0 {
        false_neg.push(label.clone());
      } else if *d <= 0.0 {
        false_pos.push(label.clone());
      }
    }
    let rate = (correct_count as f64) / (data.len() as f64);
    println!("After training, success rate is: {}", rate);
    (rate, false_pos, false_neg)
  }

  pub fn check_binary<T: Clone>(&self, data: &[(Vec<f64>, bool, T)]) -> (f64, Vec<T>, Vec<T>) {
    let mut false_neg = Vec::new();
    let mut false_pos = Vec::new();
    let mut correct_count = 0;
    for (x, d, label) in data.iter() {
      let yi = x.dot(&self.weights) + self.bias > 0.0;

      if *d == yi {
        correct_count += 1;
      } else if *d {
        false_neg.push(label.clone());
      } else if !*d {
        false_pos.push(label.clone());
      }
    }
    let rate = (correct_count as f64) / (data.len() as f64);
    println!("After training, success rate is: {}", rate);
    (rate, false_pos, false_neg)
  }
}
