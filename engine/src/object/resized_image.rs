use image::imageops::FilterType;
use serde::{Deserialize, Serialize};

use derives::{SerializedResource, Resource};
use crate::core::{
  load_image_file,
  ImageId,
  LoadError,
};
use crate::resource::{
  Resource,
  ResourceId,
  SerializedResource,
};

use super::Object;

#[derive(Serialize, Deserialize, Resource, SerializedResource)]
#[option_struct = "ResizedImageOptions"]
#[filename_generator = "filename_generator"]
pub struct ResizedImage {
  id: ImageId,
  options: ResizedImageOptions,
  pub aspect_ratio: f64,
  image: Vec<[f64; 3]>,
  x_offset: usize,
  y_offset: usize,
  width: usize,
  height: usize,
}

#[derive(Clone, Serialize, Deserialize)]
pub struct ResizedImageOptions {
  pub size: usize,
}

fn filename_generator(id: ResourceId, options: &ResizedImageOptions) -> String {
  format!("resized-image_size={}_id={}.input", options.size, id)
}

impl Object for ResizedImage {
  type ComputeError = LoadError;

  fn new(id: ImageId, options: ResizedImageOptions, filename: &str) -> Result<Self, LoadError> {
    load_image_file(filename)
      .map(|image| { image.resize(options.size as u32, options.size as u32, FilterType::Triangle) })
      .map(|image| { image.into_rgb8() })
      .map(|image| {
        // We need to convert image into array of floats
        let width = image.width() as usize;
        let height = image.height() as usize;
        let aspect_ratio = (width as f64) / (height as f64);
        let mut converted = vec![[0.0; 3]; width * height];
        for yi in 0..height {
          for xi in 0..width {
            let p = image.get_pixel(xi as u32, yi as u32).0;
            converted[yi * width + xi] = [(p[0] as f64)/256.0, (p[1] as f64)/256.0, (p[2] as f64)/256.0];
          }
        }

        // Compute offset as if it were a square image
        let x_offset = (options.size - width)/2;
        let y_offset = (options.size - height)/2;
        ResizedImage { id, options, aspect_ratio, image: converted, x_offset, y_offset, width, height }
      })
  }
}

impl ResizedImage {

  pub fn get_pixel(&self, x: usize, y: usize) -> [f64; 3] {
    self.image[((y - self.y_offset) * self.width + (x - self.x_offset)) as usize]
  }

  pub fn nonzero_x_range(&self) -> std::ops::Range<usize> {
    self.x_offset..(self.width + self.x_offset)
  }

  pub fn nonzero_y_range(&self) -> std::ops::Range<usize> {
    self.y_offset..(self.height + self.y_offset)
  }

}
