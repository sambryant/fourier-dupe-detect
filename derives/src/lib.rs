extern crate proc_macro;

use proc_macro::TokenStream;
use quote::quote;
use syn;

mod attr;

#[proc_macro_derive(SerializedResource)]
pub fn serialized_resource_derive(input: TokenStream) -> TokenStream {
  let ast = syn::parse(input).unwrap();
  impl_serialized_resource(&ast)
}

fn impl_serialized_resource(ast: &syn::DeriveInput) -> TokenStream {
  let name = &ast.ident;
  let gen = quote! {
    impl SerializedResource for #name {};
  };
  gen.into()
}

#[proc_macro_derive(Resource, attributes(option_struct, filename_generator))]
pub fn resource_derive(input: TokenStream) -> TokenStream {
  let ast: syn::DeriveInput = syn::parse(input).unwrap();
  impl_resource(&ast)
}

fn impl_resource(ast: &syn::DeriveInput) -> TokenStream {
  let name = &ast.ident;

  let option_struct = attr::extract_attribute(&ast.attrs, "option_struct");
  let filename_generator = attr::extract_attribute(&ast.attrs, "filename_generator");

  format!("
    impl Resource for {} {{
      type Options = {};

      fn get_id(&self) -> ResourceId {{
        self.id
      }}

      fn get_options(&self) -> &Self::Options {{
        &self.options
      }}

      fn canonical_name(id: ResourceId, options: &Self::Options) -> String {{
        {}(id, options)
        // let mut name = String::from(\"{}\");
        // name.push_str(\"-\");
        // name.push_str(&id.to_string());
        // name
      }}
    }}",
    name, option_struct, filename_generator, name).parse().unwrap()
}
